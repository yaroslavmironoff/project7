"""

Написать функцию square, принимающую 1 аргумент — сторону квадрата,
и возвращающую 3 значения (с помощью кортежа):
периметр квадрата, площадь квадрата и диагональ квадрата.

"""


def square(side_a):
    perimetr = 4 * side_a
    square_kvadrat = side_a * side_a
    diagonal = (side_a * side_a * 2) ** (0.5)
    return perimetr, square_kvadrat, diagonal


side = int(input())

print(f"Функция square (периметр квадрата = {square(side)[0]}, площадь квадрата = {square(side)[1]} и диагональ квадрата = {square(side)[2]})")
