"""

Написать функцию arithmetic, принимающую 3 аргумента:
первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их; если —, то вычесть; * — умножить;
/ — разделить (первое на второе).
В остальных случаях вернуть строку "Неизвестная операция".

"""


def arithmetic(num1, num2, arg):
    if arg == "+":
        res = num1 + num2
    elif arg == "-":
        res = num1 - num2
    elif arg == "*":
        res = num1 * num2
    elif arg == "/":
        if num2 != 0:
            res = num1 / num2
        else:
            res = "Деление на ноль"
    else:
        res = "Неизвестная операция"
    return res


num1 = int(input())
num2 = int(input())
arg = input()

print(arithmetic(num1, num2, arg))

