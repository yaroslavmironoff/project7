"""

Написать функцию is_year_leap, принимающую 1 аргумент — год, и возвращающую True,
если год високосный, и False иначе.

"""


def is_year_leap(year_s):
    if year_s % 4 != 0:
        return False
    elif year_s % 100 == 0:
        if year_s % 400 == 0:
            return True
        else:
            return False
    else:
        return True


if __name__ == '__main__':
    year = int(input())
    print(f'{year} год високосный?\n{is_year_leap(year)}')
