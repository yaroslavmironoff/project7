"""

Написать функцию XOR_cipher, принимающая 2 аргумента: строку, которую нужно зашифровать,
и ключ шифрования, которая возвращает строку, зашифрованную путем применения функции XOR (^)
над символами строки с ключом.
Написать также функцию XOR_uncipher, которая по зашифрованной строке и ключу восстанавливает
исходную строку.

"""


str_in = input("Ввод строки: ")
key_in = input("Ввод ключа: ")


def xor_cipher(string1, key1):
    code = []
    for i in range(len(string1)):
        code.append(ord(str_in[i]))
    key_code = 0
    for i in range(len(key1)):
        key_code = key_code + ord(key1[i])
    encode = []
    for i in range(len(code)):
        k = code[i] ^ key_code
        encode.append(k)
    return encode


print("Зашифрованная последовательность: ", xor_cipher(str_in, key_in))
encode_string = xor_cipher(str_in, key_in)


def xor_uncipher(string2, key2):
    decode = []
    key_code = 0
    for i in range(len(key2)):
        key_code = key_code + ord(key2[i])
    for i in range(len(string2)):
        l = chr(string2[i] ^ key_code)
        decode.append(l)
    result = "".join(decode)
    return result


print("Результат расшифрования: ", xor_uncipher(encode_string, key_in))

