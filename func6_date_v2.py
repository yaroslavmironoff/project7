"""

Написать функцию date, принимающую 3 аргумента — день, месяц и год.
Вернуть True, если такая дата есть в нашем календаре, и False иначе.

"""

from datetime import date


def check_date(y, m, d):
  try:
    date(y, m, d)
    return True
  except:
    return False


print(f"Дата есть в нашем календаре: ", check_date(2020, 12, 31))
print(f"Дата есть в нашем календаре: ", check_date(2000, 0, 1))
print(f"Дата есть в нашем календаре: ", check_date(0, 1, 1))
print(f"Дата есть в нашем календаре: ", check_date(2000, 2, 29))
print(f"Дата есть в нашем календаре: ", check_date(1900, 2, 29))
print(f"Дата есть в нашем календаре: ", check_date(4000, 2, 29))

