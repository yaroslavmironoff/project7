"""

Написать функцию date, принимающую 3 аргумента — день, месяц и год.
Вернуть True, если такая дата есть в нашем календаре, и False иначе.

"""
from func2_is_year_leap import is_year_leap


def date(day, month, year):
    set_months = {1: 31,
                  2: 28,
                  3: 31,
                  4: 30,
                  5: 31,
                  6: 30,
                  7: 31,
                  8: 31,
                  9: 30,
                  10: 31,
                  11: 30,
                  12: 31}
    if year in range(1, 10000) and month in range(1, 13):
        if month == 2 and is_year_leap(year) is True:
            set_months[2] = 29
        if day in range(1, set_months[month]+1):
            return True
        else:
            return False
    else:
        return False


print(f"Дата есть в нашем календаре: ", date(31, 12, 2020))
print(f"Дата есть в нашем календаре: ", date(1, 0, 2000))
print(f"Дата есть в нашем календаре: ", date(1, 1, 0))
print(f"Дата есть в нашем календаре: ", date(29, 2, 2000))
print(f"Дата есть в нашем календаре: ", date(29, 2, 1900))
print(f"Дата есть в нашем календаре: ", date(29, 2, 4000))