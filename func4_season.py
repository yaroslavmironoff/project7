"""

Написать функцию season, принимающую 1 аргумент — номер месяца (от 1 до 12),
и возвращающую время года, которому этот месяц принадлежит (зима, весна, лето или осень).

"""


def season(num_of_month_i):
    if num_of_month_i in (12, 1, 2):
        seas = "Winter"
        return seas
    elif num_of_month_i in range(3, 6):
        seas = "Spring"
        return seas
    elif num_of_month_i in range(6, 9):
        seas = "Summer"
        return seas
    elif num_of_month_i in range(9, 12):
        seas = "Autumn"
        return seas
    else:
        seas = "Out of range"
        return seas


num_of_month = int(input("Введите номер месяца: "))
print(f"Время года: {season(num_of_month)}")
